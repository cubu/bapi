# Birthday-Api

Express NodeJS API to remind users of their birthday. 

## Getting started

### Development mode

To launch the api + a postgresql container in development mode run this in the root folder:

```
docker-compose up --build
```

This will launch two containers with the configuration found on the `.env` file.

Access `http://localhost:8082` or the PORT value from the `.env` file.

You are set up to make some changes to the api and see them in real time.

### Production mode

Change the value from the TARGET variable from the `.env` file to `release` and perform the same command as before to launch a container in production mode.

### Running tests locally
On the root folder run:
```
npm install
npm run test
```

## Building & Pushing docker image

Use the convenience script at root level called `docker-build-push.sh`

```
bash docker-build-push.sh
```

## Deploying to AWS


### Requisites

All this tools are needed to perform the deployment of birthday-api and an instance of PostgreSQL RDS in AWS.

- [aws cli](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html)<details><summary>Version</summary> aws-cli/2.2.37 Python/3.8.8</details>

- [eksctl](https://docs.aws.amazon.com/eks/latest/userguide/eksctl.html)<details><summary>Version</summary> 0.66.0</details>

- [kubectl](https://kubernetes.io/docs/tasks/tools/)<details><summary>Version</summary>
Client Version: version.Info{Major:"1", Minor:"20", GitVersion:"v1.20.5", GitCommit:"6b1d87acf3c8253c123756b9e61dac642678305f", GitTreeState:"clean", BuildDate:"2021-03-18T01:10:43Z", GoVersion:"go1.15.8", Compiler:"gc", Platform:"linux/amd64"}
Server Version: version.Info{Major:"1", Minor:"18", GitVersion:"v1.18.3", GitCommit:"2e7996e3e2712684bc73f0dec0200d64eec7fe40", GitTreeState:"clean", BuildDate:"2020-05-20T12:43:34Z", GoVersion:"go1.13.9", Compiler:"gc", Platform:"linux/amd64"}</details>

- [terraform](https://learn.hashicorp.com/tutorials/terraform/install-cli) <details><summary>Version</summary>Terraform v1.0.6
on linux_amd64</details>

- [helm](https://helm.sh/docs/intro/install/)<details><summary>Version</summary>version.BuildInfo{Version:"v3.6.2", GitCommit:"ee407bdf364942bcb8e8c665f82e15aa28009b71", GitTreeState:"clean", GoVersion:"go1.16.5"}</details>

- [aws-iam-authenticator](https://docs.aws.amazon.com/eks/latest/userguide/install-aws-iam-authenticator.html)<details><summary>Version</summary>{"Version":"v0.5.0","Commit":"1cfe2a90f68381eacd7b6dcfa2bf689e76eb8b4b"}</details>

- [psql](https://www.postgresql.org/download/linux/ubuntu/)<details><summary>Version</summary>psql (PostgreSQL) 12.8 (Ubuntu 12.8-0ubuntu0.20.04.1)</details>

### Terraform infrastructure

Before deploying the helm chart for the birthday-api we need to setup the appropriate infrastructure in AWS.


```
cd terraform/
terraform init
terraform validate
terraform plan -out=plan
terraform apply plan
```

This will create a VPC with 6 subnets half of them private and the other ones public tagged apropriatelly to host an EKS kubernetes cluster.

It will also create a RDS PostgreSql instance on the public subnets(only to be able to access it publicly, for the sake of simplicity).

On top of that it will fire up an EKS control plane and 1 worker node, inside an autoscalable node group that could host up to 10 worker nodes depending on the nodes cpu/memory load. 

Finally it will run a script to create the table in the RDS instance, and deploy inside the k8s cluster the aws-load-balancer-controller helm chart which will allow us to configure aws application load balancers through k8s ingress resources.

### Actually deploying our API

Using helm and the kubeconfig file that our terraform generated inside terraform/kubeconfig_my-cluster we will install the birthday-api chart, which will be deployed with the values inside helm-chart/birthday-api/values.yaml

```
KUBECONFIG=./kubeconfig_my-cluster helm install birthday-api ./helm-chart/birthday-api  
```
Installing the pod throught a kubernetes deployement allows us to benefit from the rolling update strategy which will provide us with a zero downtime deployment of our pods. [Rolling updates](https://kubernetes.io/docs/tutorials/kubernetes-basics/update/update-intro/)

Wait for the ingress to allocate an alb and get the final url via:

```
KUBECONFIG=./kubeconfig_my-cluster kubectl get ingress  
```

Which will output something similar to this:

```
NAME           HOSTS   ADDRESS                                                                  PORTS   AGE
birthday-api   *       k8s-default-birthday-789bde295c-1549593395.eu-west-1.elb.amazonaws.com   80      20s
```
** There is a known bug in aws-load-balancer-controller where the alb could be binded to target groups outside the vpc where the cluster is deployed, if ingress is unable to start an alb, uninstall the helm release and install it again.
## Testing the deployed endpoints

GET /username/:username
```
curl --request GET \
  --url http://ingress-url-from-previous-step.eu-west-1.elb.amazonaws.com:80/username/username/whateveruser
```

PUT /username/:username
```
curl --request PUT \
  --url http://ingress-url-from-previous-step.eu-west-1.elb.amazonaws.com/username/pepes \
  --header 'content-type: application/json' \
  --data '{
	"dateOfBirth":"2019-09-01"
}'
```

## Destroying infrastructure

Before aplying `terraform destroy` please uninstall the birthday-api chart and wait some time until the ALB is deleted or else terraform won't be able to fully delete everything.

```
KUBECONFIG=./kubeconfig_my-cluster helm install birthday-api ./helm-chart/birthday-api
terraform destroy
```

## Infrastructure diagram

![Infra diagram](./eks-birthday-api.png)
## Notes

I'm fully aware that this deployment doesn't comply with the third requirement about using common sense. It's way overkill for a simple api. A simple EBS deployment, or an EC2 instance within an vpc with an internet gateway would have sufficed, but I wanted to explore AWS EKS and this was the perfect excuse for it.

## Next steps
- Use ECR instead of Dockerhub
- Setup TLS with cert-manager
- Set up ExternalDNS to link Rout53 with ALB
- Minimize docker image size
- Terraform files for another less overkill ways of deploying the api
- Set up RDS in private subnets

## License
[MIT](https://choosealicense.com/licenses/mit/)