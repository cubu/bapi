CREATE TABLE users_birthday (
    username TEXT NOT NULL,
    date_of_birth TIMESTAMP NOT NULL,
    PRIMARY KEY(username)
);