FROM node:10-alpine AS base

WORKDIR /app
COPY . /app
RUN npm ci


FROM base AS development
RUN npm install -g nodemon
CMD nodemon src/index.js

FROM base as node
WORKDIR /app
RUN npm ci --purge --only=production
RUN chown -R node:node /app
USER node

FROM gcr.io/distroless/nodejs:10 as release
COPY --from=node /app /app
CMD ["app/src/index.js"]
