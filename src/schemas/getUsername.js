const Joi = require('@hapi/joi').extend(require('@joi/date'));

const saveUsername = Joi.object({
  username: Joi.string()
    .required()
    .pattern(/^([^0-9]*)$/, { name: 'numbers not allowed' }),
});

module.exports = saveUsername;
