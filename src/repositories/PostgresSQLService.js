const { Pool } = require('pg');
const logger = require('../logger');
const { postgreSQLConfig } = require('../config');

class PostgresSQLService {
  constructor() {
    logger.info('PostgresSQLService - constructor');
    this.pool = null;
  }

  getPool() {
    if (!this.pool) {
      this.pool = new Pool(postgreSQLConfig);
    }

    return this.pool;
  }
}

module.exports = new PostgresSQLService();
