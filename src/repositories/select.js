const PostgresSQLService = require('./PostgresSQLService');

const selectQuery = {
  text: `
  SELECT username, date_of_birth FROM users_birthday WHERE username = $1;
  `,
};

const select = async ({ username }) => {
  const pool = await PostgresSQLService.getPool();
  const values = [username];
  try {
    const res = pool.query(selectQuery, values);

    return res;
  } catch (e) {
    throw e;
  }
};

module.exports = select;
