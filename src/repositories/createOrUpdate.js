const PostgresSQLService = require('./PostgresSQLService');

const createOrUpdateQuery = {
  name: 'create-or-update-username',
  text: `
  INSERT INTO users_birthday (
    username,
    date_of_birth
  )
  VALUES ($1, $2)
  ON CONFLICT (username) DO UPDATE 
  SET 
    date_of_birth = $2
  `,
};

const createOrUpdate = async ({ username, dateOfBirth }) => {
  const pool = await PostgresSQLService.getPool();
  const values = [username, dateOfBirth];

  try {
    const res = pool.query(createOrUpdateQuery, values);

    return res;
  } catch (e) {
    throw e;
  }
};

module.exports = createOrUpdate;
