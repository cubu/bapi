require('dotenv').config();
const express = require('express');
const cors = require('cors');
const { serverConfig } = require('./config/index');
const logger = require('./logger');
const app = express();

var corsOptions = {
  origin: serverConfig.corsDomain,
};
const routes = require('./routes/router');

app.use(cors(corsOptions));
app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  })
);
app.use('/', routes);

const port = parseInt(serverConfig.port);

const start = async () => {
  try {
    app.listen(port, () => {
      logger.info(`Running on ${port}`);
    });
  } catch (e) {
    console.error(e);
    process.exit(1);
  }
};

start();
