const express = require('express');
const saveUsername = require('../handlers/saveUsername');
const getUsername = require('../handlers/getUsername');

const router = new express.Router();

router.route('/username/:username').put(saveUsername);

router.route('/username/:username').get(getUsername);

module.exports = router;
