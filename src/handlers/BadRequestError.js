class BadRequestError extends Error {
  constructor(message, options = {}) {
    super(message);
    this.options = options;
  }

  get statusCode() {
    return 400;
  }
}

module.exports = BadRequestError;
