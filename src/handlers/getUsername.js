const genericHandler = require('./genericHandler');
const blGetUsername = require('../services/getUsername');
const getUsernameSchema = require('../schemas/getUsername');

const getUsername = async (req, res, next) => {
  const {
    params: { username },
  } = req;

  genericHandler({
    blFunction: blGetUsername,
    errorMsg: 'GET username/<username> error',
    res,
    req,
    httpCode: 200,
    next,
    schema: getUsernameSchema,
    body: {
      username,
    },
  })({
    username,
  });
};

module.exports = getUsername;
