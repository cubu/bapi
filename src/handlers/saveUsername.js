const genericHandler = require('./genericHandler');
const blSaveUsername = require('../services/saveUsername');
const saveUsernameSchema = require('../schemas/saveUsername');

const saveUsername = async (req, res, next) => {
  const {
    params: { username },
    body: { dateOfBirth },
  } = req;

  genericHandler({
    blFunction: blSaveUsername,
    errorMsg: 'PUT username/<username> error',
    res,
    req,
    httpCode: 204,
    next,
    schema: saveUsernameSchema,
    body: {
      username,
      dateOfBirth,
    },
  })({
    username,
    dateOfBirth,
  });
};

module.exports = saveUsername;
