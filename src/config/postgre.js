const { PG_HOST, PG_DATABASE, PG_USER, PG_PASSWORD, PG_PORT } = process.env;

const postgreSQLConfig = {
  user: PG_USER,
  password: PG_PASSWORD,
  host: PG_HOST,
  database: PG_DATABASE,
  port: PG_PORT,
  max: 10,
};

module.exports = postgreSQLConfig;
