const serverConfig = require('./server');
const postgreSQLConfig = require('./postgre');

module.exports = { serverConfig, postgreSQLConfig };
