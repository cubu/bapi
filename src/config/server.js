const { LOG_LEVEL, PORT = 8080, SECRET, CORS_DOMAIN } = process.env;

const serverConfig = {
  level: LOG_LEVEL || 'info',
  port: PORT,
  corsDomain: CORS_DOMAIN,
};

module.exports = serverConfig;
