const getUsername = require('../getUsername');
const selectMock = require('../../repositories/select');
jest.mock('../../repositories/select', () =>
  jest.fn().mockReturnValue({
    rows: [
      {
        date_of_birth: Date.now(),
        username: 'pepe',
      },
    ],
  })
);

afterEach(() => {
  jest.clearAllMocks();
});

test('Happy Birthday', async () => {
  const result = await getUsername({ username: 'pepe' });

  expect(selectMock).toHaveBeenCalled();
  expect(selectMock).toHaveBeenCalledWith({
    username: 'pepe',
  });
  expect(result.message).toBe('Hello, pepe! Happy Birthday!');
});

test('N days to your birthday in this year', async () => {
  const today = new Date();
  const tomorrow = new Date(today);
  tomorrow.setDate(tomorrow.getDate() + 1);
  selectMock.mockReturnValue({
    rows: [
      {
        date_of_birth: tomorrow,
        username: 'pepe',
      },
    ],
  });
  const result = await getUsername({ username: 'pepe' });

  expect(selectMock).toHaveBeenCalled();
  expect(selectMock).toHaveBeenCalledWith({
    username: 'pepe',
  });
  expect(result.message).toBe('Hello, pepe! Your birthday is in 1 day(s)');
});

test('N days to your birthday in next year', async () => {
  const today = new Date();
  const yesterday = new Date(today);
  yesterday.setDate(yesterday.getDate() - 1);
  selectMock.mockReturnValue({
    rows: [
      {
        date_of_birth: yesterday,
        username: 'pepe',
      },
    ],
  });
  const result = await getUsername({ username: 'pepe' });

  expect(selectMock).toHaveBeenCalled();
  expect(selectMock).toHaveBeenCalledWith({
    username: 'pepe',
  });
  expect(result.message).toBeOneOf([
    'Hello, pepe! Your birthday is in 364 day(s)',
    'Hello, pepe! Your birthday is in 365 day(s)',
  ]);
});

test('User not found', async () => {
  const today = new Date();
  const yesterday = new Date(today);
  yesterday.setDate(yesterday.getDate() - 1);
  let err = new Error();
  selectMock.mockReturnValue({
    rows: [],
  });
  try {
    const result = await getUsername({ username: 'pepe' });
  } catch (e) {
    err = e;
  }

  expect(selectMock).toHaveBeenCalled();
  expect(selectMock).toHaveBeenCalledWith({
    username: 'pepe',
  });
  expect(err.message).toBe('Username not found!');
});
