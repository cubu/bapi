const select = require('../repositories/select');
const dayjs = require('dayjs');
var dayOfYear = require('dayjs/plugin/dayOfYear');
dayjs.extend(dayOfYear);

const _daysOfYear = (year) => (_isLeapYear(year) ? 366 : 365);

const _isLeapYear = (year) => year % 400 === 0 || (year % 100 !== 0 && year % 4 === 0);

const getUsername = async ({ username }) => {
  const { rows } = await select({ username });

  if (!rows.length) {
    throw new Error('Username not found!');
  }
  const { date_of_birth: dateOfBirth } = rows[0];

  const daysUntilBirthday = dayjs(dateOfBirth).dayOfYear() - dayjs().dayOfYear();

  let userMessage = '';
  switch (Math.sign(daysUntilBirthday)) {
    case 0:
      userMessage = 'Happy Birthday!';
      break;
    case 1:
      userMessage = `Your birthday is in ${daysUntilBirthday} day(s)`;
      break;
    case -1:
      const realDaysUntilBirthday = _daysOfYear(dayjs().year) + daysUntilBirthday;
      userMessage = `Your birthday is in ${realDaysUntilBirthday} day(s)`;
      break;
  }
  const answer = {
    message: `Hello, ${username}! ${userMessage}`,
  };
  return answer;
};

module.exports = getUsername;
