const createOrUpdateUsernameBirthday = require('../repositories/createOrUpdate');

const saveUsername = async ({ username, dateOfBirth }) => {
  await createOrUpdateUsernameBirthday({ username, dateOfBirth });
};

module.exports = saveUsername;
