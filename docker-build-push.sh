#!/bin/sh
TAG="v0.1.1"
: "${TAG:?Need to set TAG non-empty}"
TARGET="release" docker build . -t christiancb/bapi:$TAG
docker push christiancb/bapi:$TAG