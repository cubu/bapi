resource "aws_iam_policy" "worker_policy" {
  name        = "worker-policy"
  description = "Worker policy for the ALB Ingress"

  policy = file("iam-policyv2.2.4.json")
}

resource "aws_iam_policy" "load-balancer-policy" {
  name        = "AWSLoadBalancerControllerIAMPolicy"
  path        = "/"
  description = "AWS LoadBalancer Controller IAM Policy"

  policy = file("iam-policyv2.2.4.json")
  
}

resource "null_resource" "post-policy" {
depends_on=[aws_iam_policy.load-balancer-policy]
triggers = {
    always_run = timestamp()
}
provisioner "local-exec" {
    on_failure  = fail
    interpreter = ["/bin/bash", "-c"]
    when = create
    command     = <<EOT
        reg=$(echo ${module.eks.cluster_arn} | cut -f4 -d':')
        acc=$(echo ${module.eks.cluster_arn} | cut -f5 -d':')
        cn=$(echo ${module.eks.cluster_id})
        kconfig=$(echo kubeconfig_${module.eks.cluster_id})
        rds_endpoint=$(echo ${module.db.db_instance_endpoint} | rev | cut -c6- | rev)
        rds_port=$(echo ${module.db.db_instance_port})
        rds_user=$(echo ${nonsensitive(module.db.db_instance_username)})
        rds_db=$(echo ${module.db.db_instance_name})
        rds_password=$(echo ${nonsensitive(module.db.db_instance_password)})
        echo "$reg $cn $acc $kconfig $rds_endpoint $rds_port $rds_user $rds_db"
        ./post-policy.sh $reg $cn $acc $kconfig $rds_endpoint $rds_port $rds_user $rds_db $rds_password
        echo "done"
     EOT
}
}
