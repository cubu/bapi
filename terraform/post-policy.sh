test -n "$1" && echo REGION is "$1" || "echo REGION is not set && exit"
test -n "$2" && echo CLUSTER is "$2" || "echo CLUSTER is not set && exit"
test -n "$3" && echo ACCOUNT is "$3" || "echo ACCOUNT is not set && exit"
test -n "$4" && echo KUBECONFIG_PATH is "$4" || "echo KUBECONFIG_PATH is not set && exit"
test -n "$5" && echo RDS_ENDPOINT is "$4" || "echo RDS_ENDPOINT is not set && exit"
test -n "$6" && echo RDS_PORT is "$4" || "echo RDS_PORT is not set && exit"
test -n "$7" && echo RDS_USER is "$4" || "echo RDS_USER is not set && exit"
test -n "$8" && echo RDS_DB is "$4" || "echo RDS_DB is not set && exit"
test -n "$9" && echo RDS_PWD is "$4" || "echo RDS_PWD is not set && exit"
PGPASSWORD=$9 psql -h $5 -p $6 -U $7 -d $8 -f "../initdb/01_users_birthday.sql"    
KUBECONFIG=$4 helm repo add eks https://aws.github.io/eks-charts
rm -f crds.yaml*
wget https://raw.githubusercontent.com/aws/eks-charts/master/stable/aws-load-balancer-controller/crds/crds.yaml
# create the custom resource definition for the load balancer
KUBECONFIG=$4 kubectl apply -f crds.yaml
# install the load balancer controller using the helm chart 
KUBECONFIG=$4 helm upgrade -i aws-load-balancer-controller eks/aws-load-balancer-controller -n kube-system --set clusterName=$2 --set serviceAccount.name=aws-load-balancer-controller --set image.repository=602401143452.dkr.ecr.$1.amazonaws.com/amazon/aws-load-balancer-controller --set image.tag="v2.2.4"